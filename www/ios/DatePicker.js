
/**
  Phonegap DatePicker Plugin
  https://github.com/sectore/phonegap3-ios-datepicker-plugin
  
  Copyright (c) Greg Allen 2011
  Additional refactoring by Sam de Freyssinet
  
  Rewrite by Jens Krause (www.websector.de)

  MIT Licensed
*/

var exec = require('cordova/exec');
/**
 * Constructor
 */
function DatePicker() {
    this._callback;
}

/**
 * show - true to show the ad, false to hide the ad
 */
DatePicker.prototype.show = function(options, cb) {
    var padDate = function(date) {
      if (date.length == 1) {
        return ("0" + date);
      }
      return date;
    };

    var formatDate = function(date){
      date = date.getFullYear() 
            + "-" 
            + padDate(date.getMonth()+1) 
            + "-" 
            + padDate(date.getDate()) 
           // + "T"
           // + padDate(date.getHours())
           // + ":"
           // + padDate(date.getMinutes())
           // + ":00Z";

      return date
    }

    if (options.date) {
        //options.date = formatDate(options.date);
               options.date = options.date.getFullYear() + "-" +
               padDate(options.date.getMonth()+1) + "-" +
               padDate(options.date.getDate())
    }

    if (/*options.minDate*/options.maximumDate && options.maximumDate != "" && options.mode == "date") {
        //options.minDate = formatDate(options.minDate);
        options.maximumDate = options.maximumDate.getFullYear() + "-" +
        padDate(options.maximumDate.getMonth()+1) + "-" +
        padDate(options.maximumDate.getDate())
       
    }

    if (options.minimumDate && options.minimumDate != "" && options.mode == "date" /*options.maxDate*/) {
        //options.maxDate = formatDate(options.maxDate);
        options.minimumDate = options.minimumDate.getFullYear() + "-" +
        padDate(options.minimumDate.getMonth()+1) + "-" +
        padDate(options.minimumDate.getDate())
               
    }

    var defaults = {
        mode: 'date',
        //date: new Date(),
        date: '',
        allowOldDates: true,
        allowFutureDates: true,
        minimumDate: '',
        maximumDate: '',
        doneButtonLabel: 'Done',
        doneButtonColor: '#0000FF',
        cancelButtonLabel: 'Cancel',
        cancelButtonColor: '#000000',
        x: '0',
        y: '0'
    };

    for (var key in defaults) {
        if (typeof options[key] !== "undefined")
            defaults[key] = options[key];
    }
    this._callback = cb;
              
    exec(cb,
      failureCallback,
      "DatePicker", 
      "show",
      [defaults]
    );
};

DatePicker.prototype._dateSelected = function(date) {
              
    var d = date;//new Date(parseFloat(date) * 1000);
    if (this._callback)
        this._callback(d);
}
               
function failureCallback(err) {
    console.log("datePicker.js failed: " + err);
}

var datePicker = new DatePicker();
module.exports = datePicker;

// Make plugin work under window.plugins
if (!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.datePicker) {
    window.plugins.datePicker = datePicker;
}
