 /**
 * Phonegap DatePicker Plugin Copyright (c) Greg Allen 2011 MIT Licensed
 * Reused and ported to Android plugin by Daniel van 't Oever
 * Revised for phonegap 3.0.0 by Patrick Foh
 */
var DatePicker = (function (gap) {
	/**
	 * Constructor
	 */
	function DatePicker() {
		this._callback;
	}

	/**
	 * show - true to show the ad, false to hide the ad
	 */
	DatePicker.prototype.show = function(options, cb) {
        var padDate = function(date) {
          if (date.length == 1) {
            return ("0" + date);
          }
          return date;
        };

        if (options.date) {
            options.date = options.date.getFullYear() + "-" +
                           padDate(options.date.getMonth()+1) + "-" +
                           padDate(options.date.getDate()) 
                           //+" " + padDate(options.date.getHours()) + ":" +
                          // padDate(options.date.getMinutes()) //+ ":00Z"
                           ;
        }


        if (options.maximumDate && options.maximumDate != "" && options.mode == "date") {
            options.maximumDate = options.maximumDate.getFullYear() + "-" +
                           padDate(options.maximumDate.getMonth()+1) + "-" +
                           padDate(options.maximumDate.getDate()) 
//                           +"T" + padDate(options.maximumDate.getHours()) + ":" +
//                           padDate(options.maximumDate.getMinutes()) + ":00Z"
                           ;
        }

        if (options.minimumDate && options.minimumDate != "" && options.mode == "date") {
            options.minimumDate = options.minimumDate.getFullYear() + "-" +
                           padDate(options.minimumDate.getMonth()+1) + "-" +
                           padDate(options.minimumDate.getDate()) 
//                           +"T" + padDate(options.minimumDate.getHours()) + ":" +
//                           padDate(options.minimumDate.getMinutes()) + ":00Z"
                           ;
        }

		
		var defaults;
		
		defaults = {
			mode : 'date',
			date : '',
			maximumDate : '',
			minimumDate : ''
		};


        for (var key in defaults) {
            if (typeof options[key] !== "undefined") {
                defaults[key] = options[key];
            }
        }

        this._callback = cb;
     //   alert("call");
        console.log("DatePickerCalled");
	//	if(isMobile.Windows())
	//	    cordova.exec(cb, failureCallback, "DatePicker", "show", new Array(defaults));
	//	else
			cordova.exec(cb,failureCallback,"DatePicker","show",new Array(defaults));		
     //   cordova.exec("DatePicker.show", defaults);
    };

	DatePicker.prototype._dateSelected = function(date) {
    	// console.log("return");
    	// console.log(parseFloat(date));
    	
    	var d = date;
    		
    	//if(date != ""){
    	//	 d = new Date(parseFloat(date) * 1000);
    	//}
       
    	// console.log("final "+d+"<");
    	
        if (this._callback)
            this._callback(d);
    }

	function failureCallback(err) {
		console.log("DatePicker.js failed: " + err);
	}

	/**
     * Load DatePicker
     */
    gap.addConstructor(function () {
        if (gap.addPlugin) {
            gap.addPlugin("DatePicker", DatePicker);
        } else {
            if (!window.plugins) {
                window.plugins = {};
            }

            window.plugins.datePicker = new DatePicker();
        }
    });

	return DatePicker;


})(window.cordova || window.Cordova || window.PhoneGap);
