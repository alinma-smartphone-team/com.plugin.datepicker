﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPCordovaClassLib.Cordova;
using WPCordovaClassLib.Cordova.Commands;
using WPCordovaClassLib.Cordova.JSON;
using System.Runtime.Serialization;
using System.Windows;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace Cordova.Extension.Commands
{
    public class DatePicker : BaseCommand
    {
        #region DateTimePicker Options

        /// <summary>
        /// Represents DateTimePicker options
        /// </summary>
        [DataContract]
        public class DateTimePickerOptions
        {
            /// <summary>
            /// Initial value for time or date
            /// </summary>
            [DataMember(IsRequired = false, Name = "mode")]
            public String Mode { get; set; }  

            [DataMember(IsRequired = false, Name = "date")]
            public DateTime Date { get; set; } 

            [DataMember(IsRequired = false, Name = "maximumDate")]
            public String MaximumDate { get; set; } 

            [DataMember(IsRequired = false, Name = "minimumDate")]
            public String MinimumDate { get; set; }
 
     
                        /// <summary>
            /// Creates options object with default parameters
            /// </summary>
            public DateTimePickerOptions()
            {
                this.SetDefaultValues(new StreamingContext());
            }

            /// <summary>
            /// Initializes default values for class fields.
            /// Implemented in separate method because default constructor is not invoked during deserialization.
            /// </summary>
            /// <param name="context"></param>
            [OnDeserializing()]
            public void SetDefaultValues(StreamingContext context)
            {
                this.Date = DateTime.Now;
                //this.MaximumDate = DateTime.Now;
            }

        }
        #endregion

        private DateTimePickerTask dateTimePickerTask;

        private DateTimePickerOptions dateTimePickerOptions;

        public void show(string options)
        {

            try
            {

                try
                {
					String[] parameters = JsonHelper.Deserialize<String[]>(options);
							
                    this.dateTimePickerOptions = String.IsNullOrEmpty(parameters[0]) ? new DateTimePickerOptions() :
                            JsonHelper.Deserialize<DateTimePickerOptions>(parameters[0]);
                }
                catch (Exception ex)
                {
                    // simply catch the exception, we will handle null values and exceptions together
                    this.DispatchCommandResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, ex.Message));
                    return;
                }

                this.dateTimePickerTask = new DateTimePickerTask();
                dateTimePickerTask.Value = dateTimePickerOptions.Date;

                dateTimePickerTask.Completed += this.dateTimePickerTask_Completed;
                dateTimePickerTask.Show(DateTimePickerTask.DateTimePickerType.Date);
            }
            catch (Exception e)
            {
                DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, e.Message));
            }

        }

        /// <summary>
        /// Triggers  special UI to select a time (hour/minute/am/pm).
        /// </summary>
        public void selectTime(string options)
        {

            try
            {
                try
                {
                    this.dateTimePickerOptions = String.IsNullOrEmpty(options) ? new DateTimePickerOptions() :
                        JsonHelper.Deserialize<DateTimePickerOptions>(options);

                }
                catch (Exception ex)
                {
                    this.DispatchCommandResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, ex.Message));
                    return;
                }

                this.dateTimePickerTask = new DateTimePickerTask();
                dateTimePickerTask.Value = dateTimePickerOptions.Date;

                dateTimePickerTask.Completed += this.dateTimePickerTask_Completed;
                dateTimePickerTask.Show(DateTimePickerTask.DateTimePickerType.Time);
            }
            catch (Exception e)
            {
                DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, e.Message));
            }
        }

        /// <summary>
        /// Handles datetime picker result
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">stores information about current captured image</param>
        private void dateTimePickerTask_Completed(object sender, DateTimePickerTask.DateTimeResult e)
        {

            if (e.Error != null)
            {
                DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR));
                return;
            }

            switch (e.TaskResult)
            {
                case TaskResult.OK:
                    try
                    {
                        String result = e.Value.Value.ToString("MM-dd-yyyy");
                        DispatchCommandResult(new PluginResult(PluginResult.Status.OK, result));
                    }
                    catch (Exception ex)
                    {
                        DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, "Datetime picker error. " + ex.Message));
                    }
                    break;

                case TaskResult.Cancel:
                    DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, "Canceled."));
                    break;
            }

            this.dateTimePickerTask = null;
        }      
    }
}
